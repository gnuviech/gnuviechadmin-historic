# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

from setuptools import setup, find_packages

try:
    import buildutils
except ImportError:
    pass

setup(
    name = 'gnuviechadmin',
    version = '0.1',
    description = 'gnuviechadmin server administration suite',
    author = 'Jan Dittberner',
    author_email = 'jan@dittberner.info',
    url = 'http://www.gnuviech-server.de/projects/gnuviechadmin',
    install_requires = ['SQLAlchemy>=0.5', 'sqlalchemy-migrate>=0.5',
                        'AuthKit>=0.4', 'PasteDeploy>=1.3.3',
                        'python-gnutls>=1.1',
			'argparse'],
    packages = find_packages(),
    include_package_data = True,
    exclude_package_data = {'': ['gva.cfg']},
    test_suite='nose.collector',
    scripts = ['bin/gva', 'bin/gvaserver'],
    long_description = """this is a suite of tools for administering a server
it contains tools for maintaining e.g. clients, domains, users, mail
accounts""",
    license = 'GPL',
    keywords = 'administration backend frontend',
    entry_points = """
[paste.app_factory]
cli = gnuviechadmin.cli.client
""",
)
