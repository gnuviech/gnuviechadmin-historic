# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""This module provides the command line interface code for system
user management."""
from gnuviechadmin.cli.CliCommand import CliCommand
import sys


class SysuserCli(CliCommand):
    """Command line interface command for system user management.

    This class implements `gnuviechadmin.cli.CliCommand.CliCommand`.
    """

    @staticmethod
    def setup_argparser(subparsers):
        parser = subparsers.add_parser(
            'sysuser',
            help = _('manage system users'))
        parser.set_defaults(commandclass=SysuserCli)
        cmdsub = parser.add_subparsers(
            title = _('sysuser subcommands'),
            dest = 'subcommand')
        cmdparser = cmdsub.add_parser(
            'create',
            help = _('create a new system user with the given options.'))
        cmdparser.add_argument(
            '-n', '--username',
            help = _('the system user name'))
        cmdparser.add_argument(
            '-t', '--usertype',
            help = _('the numeric user type'))
        cmdparser.add_argument(
            '--home',
            help = _('the home directory'))
        cmdparser.add_argument(
            '-s', '--shell',
            help = _('true if the user should get shell access'))
        cmdparser.add_argument(
            '-p', '--password', dest = 'clearpass',
            help = _('the password for the user'))
        cmdparser.add_argument(
            '-c', '--clientid', required = True,
            help = _('the client id'))
        cmdparser = cmdsub.add_parser(
            'list',
            help = _('list existing system users.'))
        cmdparser = cmdsub.add_parser(
            'delete',
            help = _('delete a system user.'))
        cmdparser.add_argument(
            '-s', '--sysuserid', required = True,
            help = _('the system user id'))

    def _execute(self):
        """Executes `subcommand`.

        This method implements `CliCommand._execute()`.
        """
        self.logger.debug("execute %s", self.args)
        from gnuviechadmin.backend import sysuser
        from gnuviechadmin import exceptions
        sh = sysuser.SysuserHandler(self.config, self.args.verbose)
        if self.args.subcommand == "create":
            try:
                mysysuser = sh.create(**self.args.__dict__)
                if self.args.verbose:
                    print mysysuser
            except exceptions.CreationFailedError, cfe:
                print cfe
                sys.exit(2)
        elif self.args.subcommand == "list":
            sysusers = sh.fetchall()
            for su in sysusers:
                print su
        elif self.args.subcommand == "delete":
            sh.delete(self.args.sysuserid)

    def __init__(self, args, config):
        """Constructor for the sysuser command."""
        CliCommand.__init__(self, args, config)
