# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
import argparse
import sys
import logging
from gnuviechadmin.exceptions import GnuviechadminError


class CliCommand(object):
    """
    Base class for command line interface.

    A specific implementation class must implement the static
    setup_arparser method and the _execute method.
    """

    @staticmethod
    def setup_argparser(subparsers):
        """
        This static method is used to initialize the command line
        argument parser.

        The method has to be overridden by subclasses.
        """
        raise NotImplementedError


    def _execute(self):
        """
        This method is called when the subcommand of the command is
        executed.

        The method has to be overridden by subclasses.
        """
        raise NotImplementedError

    def __init__(self, args, config):
        """
        This initializes the command with the given command line
        arguments and executes it.
        """
        self.config = config
        self.args = args
        self.logger = logging.getLogger("%s.%s" % (
            self.__class__.__module__, self.__class__.__name__))
        self._execute()
