# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""This is the gnuviechadmin.cli package.

This package provides modules for the command line interface of the
gnuviechadmin server administration suite."""

__all__ = ["client", "sysuser", "domain", "record"]

from logging import getLogger
from sys import exit
from argparse import ArgumentParser
import gettext


class CommandLineInterface(object):
    def __init__(self, config, args):
        gettext.install('gnuviechadmin')
        self.log = getLogger(__name__)
        self.config = config
        parser = ArgumentParser(prog=args[0])
        parser.add_argument(
            '-v', '--verbose',
            help = _('enable verbose output'),
            action = 'store_true')
        subparsers = parser.add_subparsers(
            title = _('subcommands'), dest  = 'command')
        from gnuviechadmin.cli.client import ClientCli
        from gnuviechadmin.cli.sysuser import SysuserCli
        from gnuviechadmin.cli.domain import DomainCli
        from gnuviechadmin.cli.record import RecordCli
        for command in [ClientCli, SysuserCli]:
            command.setup_argparser(subparsers)
        self.parsedargs = parser.parse_args(args[1:])
        
    def run(self):
        self.parsedargs.commandclass(self.parsedargs, self.config)
