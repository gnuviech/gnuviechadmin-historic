# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
import CliCommand
import sys


class RecordCli(CliCommand.CliCommand):
    """Command line interface command for DNS record management."""

    name = "record"
    description = "manage DNS records"
    _optionmap = {
        'create': ("creates a new record",
                   [(["-n", "--name"], "name",
                     "the record name", True),
                    (["-t", "--type"], "type",
                     "record type", True),
                    (["-c", "--content"], "content",
                     "record content", True),
                    (["-p", "--prio"], "prio",
                     "MX record priority", False),
                    (["--ttl"], "ttl",
                     "time to live", False),
                    (["-d", "--domainid"], "domainid",
                     "domain id", True)]),
        'list': ("lists existing records",
                 [(["-d", "--domainid"], "domainid",
                   "domain id", False)]),
        'delete': ("delete a record",
                   [(["-r", "--recordid"], "recordid",
                     "the record id", True)])}

    def _execute(self, subcommand):
        self.logger.debug("execute %s with data %s", subcommand,
                          str(self._data))
        from gnuviechadmin.backend.record import RecordHandler
        from gnuviechadmin import exceptions
        if subcommand == "create":
            try:
                myrecord = RecordHandler(self._verbose).create(
                    **self._data)
                if self._verbose:
                    print myrecord
            except exceptions.CreationFailedError, cfe:
                self._usage()
                print cfe
                sys.exit(2)
        elif subcommand == "list":
            records = RecordHandler(self._verbose).fetchall(**self._data)
            for record in records:
                print record
        elif subcommand == "delete":
            RecordHandler(self._verbose).delete(self._data["recordid"])

    def __init__(self, argv):
        CliCommand.CliCommand.__init__(self, argv)
