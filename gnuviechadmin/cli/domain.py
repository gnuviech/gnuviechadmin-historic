# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
import CliCommand
import sys


class DomainCli(CliCommand.CliCommand):
    """Command line interface command for domain management."""

    name = "domain"
    description = "manage domains"
    _optionmap = {
        'create': ("creates a new domain",
                   [(["-n", "--name"], "name",
                     "the domain name", True),
                    (["-t", "--type"], "type",
                     "domain type m for master or s for slave", False),
                    (["-m", "--master"], "master",
                     "master server for slave domains", False),
                    (["-s", "--sysuserid"], "sysuserid",
                     "system user id", True)]),
        'list': ("lists existing domains", []),
        'delete': ("delete a domain",
                   [(["-d", "--domainid"], "domainid",
                     "the domain id", True)])}

    def _execute(self, subcommand):
        self.logger.debug("execute %s with data %s", subcommand,
                          str(self._data))
        from gnuviechadmin.backend.domain import DomainHandler
        from gnuviechadmin import exceptions
        if subcommand == "create":
            try:
                mydomain = DomainHandler(self._verbose).create(
                    **self._data)
                if self._verbose:
                    print mydomain
            except exceptions.CreationFailedError, cfe:
                self._usage()
                print cfe
                sys.exit(2)
        elif subcommand == "list":
            domains = DomainHandler(self._verbose).fetchall()
            for domain in domains:
                print domain
        elif subcommand == "delete":
            DomainHandler(self._verbose).delete(self._data["domainid"])

    def __init__(self, argv):
        CliCommand.CliCommand.__init__(self, argv)
