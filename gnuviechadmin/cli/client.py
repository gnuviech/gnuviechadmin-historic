# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
import CliCommand
import sys


class ClientCli(CliCommand.CliCommand):
    """Command line interface command for client management."""

    @staticmethod
    def setup_argparser(subparsers):
        parser = subparsers.add_parser(
            'client',
            help = _('manage clients'))
        parser.set_defaults(commandclass=ClientCli)
        cmdsub = parser.add_subparsers(
            title = _('client subcommands'), dest = 'subcommand')
        cmdparser = cmdsub.add_parser(
            'create',
            help = _('creates a new client'))
        cmdparser.add_argument(
            '-f', '--firstname', required = True,
            help = _("the client's first name"))
        cmdparser.add_argument(
            '-l', '--lastname', required = True,
            help = _("the client's last name"))
        cmdparser.add_argument(
            '-t', '--title',
            help = _("the client's title"))
        cmdparser.add_argument(
            '-a', '--address', dest = 'address1', required = True,
            help = _("the address of the client"))
        cmdparser.add_argument(
            '--address2',
            help = _("second line of the client's address"))
        cmdparser.add_argument(
            '-z', '--zip', required = True,
            help = _("the zipcode of the client's address"))
        cmdparser.add_argument(
            '-c', '--city', required = True,
            help = _("the city of the client's address"))
        cmdparser.add_argument(
            '--country',
            help = _("the client's country"))
        cmdparser.add_argument(
            '-e', '--email', required = True,
            help = _("the client's email address"))
        cmdparser.add_argument(
            '-p', '--phone', required = True,
            help = _("the client's phone number"))
        cmdparser.add_argument(
            '-m', '--mobile',
            help = _("the client's mobile phone number"))
        cmdparser.add_argument(
            '-x', '--fax',
            help = _("the client's fax number"))
        cmdparser = cmdsub.add_parser(
            'list',
            help = _('lists existing clients'))
        cmdparser = cmdsub.add_parser(
            'delete',
            help = _(
                'deletes the specified client if it has no dependent data'))
        cmdparser.add_argument(
            '-c', '--clientid', required = True,
            help = _("the client id"))

    def _execute(self):
        self.logger.debug("execute %s", self.args)
        from gnuviechadmin.backend.client import ClientHandler
        from gnuviechadmin.exceptions import CreationFailedError
        ch = ClientHandler(self.config, self.args.verbose)
        if self.args.subcommand == "create":
            try:
                myclient = ch.create(**self.args.__dict__)
                if self.args.verbose:
                    print unicode(myclient).encode('utf8')
            except CreationFailedError, cfe:
                print cfe
                sys.exit(2)
        elif self.args.subcommand == "list":
            clients = ch.fetchall()
            for client in clients:
                print unicode(client).encode('utf8')
        elif self.args.subcommand == "delete":
            ch.delete(self.args.clientid)

    def __init__(self, args, config):
        CliCommand.CliCommand.__init__(self, args, config)
