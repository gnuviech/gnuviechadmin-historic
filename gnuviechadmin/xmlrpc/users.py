# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""This file provides a authkit.users.Users implementation and several
UserProviders for authenticating different user types and enabling
password change functions."""

from authkit.users import Users, AuthKitNoSuchUserError
import logging

log = logging.getLogger(__name__)


class UserProvider(Users):
    """A base class for user providers."""

    def _get_user(self, username, role):
        return {
            'username': username,
            'group': None,
            'password': None,
            'roles': [role]}


class ClientUserProvider(UserProvider):
    """A UserProvider implementation class for clients."""

    def user(self, username):
        print 'checking %s' % username
        if username == 'dummy':
            return self._get_user(username, 'client')
        raise AuthKitNoSuchUserError()

    def list_roles(self):
        return ['client']

    def role_exists(self, role):
        return 'client' == role


class MailuserUserProvider(UserProvider):
    """A UserProvider implementation class for mail users."""

    def user(self, username):
        raise AuthKitNoSuchUserError()

    def list_roles(self):
        return ['mailuser']

    def role_exists(self, role):
        return 'mailuser' == role


class SysuserUserProvider(UserProvider):
    """A UserProvider implementation class for system users."""

    def user(self, username):
        raise AuthKitNoSuchUserError()

    def list_roles(self):
        return ['sysuser']

    def role_exists(self, role):
        return 'sysuser' == role


class GVAUsers(Users):
    """This class provides an implementation of authkit.users.Users
    which dispatches several methods to configured UserProvider
    implementations."""

    def __init__(self, data, userproviders = [], encrypt = None):
        """Initialize the GVAUsers instance."""
        Users.__init__(self, data, encrypt)
        self.userproviders = [prov(self.data) for prov in userproviders]

    def list_roles(self):
        """Returns a lowercase list of all role names ordered
        alphabetically."""
        roles = []
        for prov in self.userproviders:
            for role in prov.list_roles():
                if not role in roles:
                    roles.append(role)
        roles.sort()
        return roles

    def role_exists(self, role):
        """Returns ``True`` if the role exists, ``False``
        otherwise. Roles are case insensitive."""
        for prov in self.userproviders:
            if prov.role_exists(role):
                return True
        return False

    def user(self, username):
        """Returns a dictionary in the following format:

        .. code-block :: Python

        {
          'username': username,
          'group':    group,
          'password': password,
          'roles':    [role1,role2,role3... etc]
        }

        The role names are ordered alphabetically
        Raises an exception if the user doesn't exist."""
        for prov in self.userproviders:
            try:
                return prov.user(username)
            except Exception, e:
                log.debug("Backend %s didn't find user %s" % (prov,
                                                              username))
        raise AuthKitNoSuchUserError()
