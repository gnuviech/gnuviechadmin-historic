# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""This module defines the BackendEntity base class."""

import os
import logging
import tempfile

from gnuviechadmin.exceptions import MissingFieldsError
from gnuviechadmin.util import gpgmail
from subprocess import Popen, PIPE
from sqlalchemy.orm import object_mapper


class BackendEntity(object):
    """This is the abstract base class for all backend entity classes."""

    def __init__(self, config, delegateto, verbose = False):
        self.logger = logging.getLogger("%s.%s" % (
            self.__class__.__module__, self.__class__.__name__))
        self.config = config
        self.delegateto = delegateto
        self.verbose = verbose

    def __repr__(self):
        return self.delegateto.__repr__(verbose = self.verbose)

    def sucommand(self, cmdline, pipedata = None):
        """Executes a command as root using the configured suwrapper
        command. If a pipe is specified it is used as stdin of the
        subprocess."""
        self.logger.debug("sucommand called: %s (pipedata=%s)", cmdline,
                          str(pipedata))
        suwrapper = self.config['suwrapper']
        toexec = "%s %s" % (suwrapper, cmdline)
        if pipedata:
            pipeproc = Popen(toexec, shell = True, stdin=PIPE)
            pipe = pipeproc.stdin
            print >> pipe, pipedata
            pipe.close()
            sts = os.waitpid(pipeproc.pid, 0)
            if self.verbose:
                print "%s|%s: %d" % (pipedata, toexec, sts[1])
            self.logger.info("%s|%s: %d", pipedata, toexec, sts[1])
        else:
            pipeproc = Popen(toexec, shell = True)
            sts = os.waitpid(pipeproc.pid, 0)
            if self.verbose:
                print "%s: %s" % (toexec, sts[1])
            self.logger.info("%s: %s", toexec, sts[1])
        return sts[1]

    def supipe(self, cmdlines):
        """Executes multiple commands as root and pipes the output of
        the commands to the input of the next commands."""
        self.logger.debug("supipe called: %s", " | ".join(cmdlines))
        suwrapper = self.config['suwrapper']
        predecessor = None
        for cmdline in cmdlines:
            toexec = "%s %s" % (suwrapper, cmdline)
            if predecessor is None:
                pipeproc = Popen(toexec, shell = True, stdout = PIPE)
            else:
                pipeproc = Popen(toexec, shell = True,
                                 stdin = predecessor.stdout, stdout = PIPE)
            predecessor = pipeproc
        output = predecessor.communicate()[0]
        return predecessor.wait()

    def send_mail(self, subject, text):
        """This method sends a mail with the given text and subject
        and signs it usign GnuPG. If a public key of the recipient is
        available the mail is encrypted."""
        gpgmail.send_mail(self.config, subject, text)

    def validate(self):
        """Validates whether all mandatory fields of the entity have
        values."""
        missingfields = []
        for key in [col.name for col in \
                        object_mapper(
            self.delegateto).local_table.columns \
                    if not col.primary_key and not col.nullable]:
            if self.delegateto.__getattribute__(key) is None:
                missingfields.append(key)
        if missingfields:
            raise MissingFieldsError(missingfields)

    def write_to_file(self, filename, template):
        """Write the data from template to the specified file."""
        tmp = tempfile.NamedTemporaryFile()
        tmp.write(template.encode('utf_8'))
        tmp.flush()
        cmd = 'cp "%s" "%s"' % (tmp.name, filename)
        self.sucommand(cmd)
        tmp.close()
