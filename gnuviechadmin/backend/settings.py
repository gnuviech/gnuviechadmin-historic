# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

"""The Gnuviech Admin settings module.

This module handles all central configuration of Gnuviech Admin. It
parses configuration files and provides functions for reading
templates."""

import os
from string import Template


def get_template(templatedir, filename):
    """Returns the template data from the given template file."""
    templatefile = file(os.path.join(templatedir,
                                     filename))
    templatedata = templatefile.read()
    return Template(templatedata.decode('utf_8'))


def get_template_string(templatestring):
    """Returns a template object for the given template string."""
    return Template(templatestring)
