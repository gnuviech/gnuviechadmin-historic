# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

from sqlalchemy.orm import object_mapper
from sqlalchemy import Unicode


class BackendTo(object):
    """Backend transfer object class."""

    def __init__(self, config, **kwargs):
        cols = object_mapper(self).local_table.columns
        for (key, value) in kwargs.items():
            if key in cols.keys() and value is not None:
                if isinstance(cols[key].type, Unicode):
                    self.__setattr__(key, value.decode('utf8'))
                else:
                    self.__setattr__(key, value)

    def __repr__(self, **kwargs):
        if 'verbose' in kwargs and kwargs['verbose']:
            cols = object_mapper(self).local_table.columns.keys()
            format = "%(class)s:"
            format = format + ", ".join([col + "=%(" + col + ")s" for col in \
                                         cols])
            data = {'class': self.__class__.__name__}
        else:
            cols = self._shortkeys
            format = ",".join("%(" + col + ")s" for col in cols)
            data = {}
        data.update(dict([(col, self.__getattribute__(col)) for col in cols]))
        return format % data


class Client(BackendTo):
    """Transfer object class for clients."""
    _shortkeys = ('clientid', 'firstname', 'lastname', 'email')


class Sysuser(BackendTo):
    """Transfer object class for system users."""
    _shortkeys = ("sysuserid", "clientid", "username", "home", "shell")


class Domain(BackendTo):
    """Transfer object class for DNS domains."""
    _shortkeys = ("domainid", "sysuserid", "name", "type")


class Record(BackendTo):
    """Transfer object class for DNS domain records."""
    _shortkeys = ("recordid", "domainid", "name", "type", "content")
