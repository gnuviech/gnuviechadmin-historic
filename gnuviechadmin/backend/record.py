# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

from gnuviechadmin.exceptions import *
from BackendTo import Record, Domain
from domain import DomainEntity
from BackendEntity import *
from BackendEntityHandler import *


class RecordEntity(BackendEntity):
    """Entity class for DNS domain records."""

    def create_hook(self, session):
        domain = session.load(Domain, self.delegateto.domainid)
        DomainEntity(domain).update_serial(session)

    def delete_hook(self, session):
        domain = session.load(Domain, self.delegateto.domainid)
        DomainEntity(domain).update_serial(session)


class RecordHandler(BackendEntityHandler):
    """BackendEntityHandler for Record entities."""

    def __init__(self, verbose = False):
        BackendEntityHandler.__init__(self, RecordEntity, Record, verbose)
