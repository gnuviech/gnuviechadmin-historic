# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

from sqlalchemy import MetaData, Table, Column, Unicode
from sqlalchemy.orm import mapper, relation
from sqlalchemy.exceptions import NoSuchTableError
import sys
import migrate.versioning.api
import logging
from gnuviechadmin.backend.BackendTo import Client, Sysuser, Domain, Record


def dbsetup(config):
    logger = logging.getLogger(__name__)
    required_version = int(config['migrate.required_version'])
    try:
        dbversion = migrate.versioning.api.db_version(
            config['sqlalchemy.uri'], config['database.repository'])
        if dbversion < required_version:
            logger.info("""Database version is %d but required version \
is %d. Trying automatic upgrade.""" % (dbversion, required_version))
            try:
                migrate.versioning.api.upgrade(
                    config['sqlalchemy.uri'], config['database.repository'],
                    required_version)
            except e:
                logger.error("Automatic upgrade failed.", e)
                raise
        elif dbversion > required_version:
            logger.error("""Database version is %d which is higher than \
the required version %d. I cannot handle this situation without possible \
data loss.""" % (dbversion, required_version))
            sys.exit(1)
    except NoSuchTableError, nste:
        logger.info("""The database is not versioned. \
Trying automatic versioning.""")
        try:
            migrate.versioning.api.version_control(
                config['sqlalchemy.uri'], config['database.repository'])
            migrate.versioning.api.upgrade(
                config['sqlalchemy.uri'], config['database.repository'],
                required_version)
        except:
            logger.error("Automatic setup failed.")
            raise

    meta = MetaData(config['sqlalchemy.uri'])
    meta.bind.engine.echo = config['sqlalchemy.echo']

    dbschema = None
    if 'database.schema' in config:
        dbschema = config['database.schema']

    organization_table = Table(
        'organization', meta,
        Column('name', Unicode(200)),
        schema = dbschema, autoload = True)
    client_table = Table(
        'client', meta,
        Column('title', Unicode(10)),
        Column('firstname', Unicode(64)),
        Column('lastname', Unicode(64)),
        Column('address1', Unicode(64)),
        Column('address2', Unicode(64)),
        Column('city', Unicode(64)),
        schema = dbschema, autoload = True)
    
    (sysuser_table, domain_table, \
         record_table, supermaster_table, mailaccount_table, \
         mailaddress_table, mailtarget_table) = \
         [Table(tabname, meta, schema = dbschema,
                autoload = True) for tabname in \
              ('sysuser', 'domain', 'record',
               'supermaster', 'mailaccount', 'mailaddress', 'mailtarget')]

    client_mapper = mapper(Client, client_table, {
            'sysusers': relation(Sysuser, backref = 'client')})
    sysuser_mapper = mapper(Sysuser, sysuser_table, {
            'domains': relation(Domain, backref = 'sysuser')})
    domain_mapper = mapper(Domain, domain_table, {
            'records': relation(Record, cascade = 'all',
                                backref = 'domain')})
    record_mapper = mapper(Record, record_table)
