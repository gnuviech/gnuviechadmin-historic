# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008, 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""This module defines the ClientEntity class."""

from gnuviechadmin.backend.settings import get_template, get_template_string
from gnuviechadmin.exceptions import CannotDeleteError
from gnuviechadmin.backend.BackendTo import Client
from gnuviechadmin.backend.BackendEntity import BackendEntity
from gnuviechadmin.backend.BackendEntityHandler import BackendEntityHandler


class ClientEntity(BackendEntity):
    """Entity class for clients."""

    def __init__(self, config, delegate, verbose = False, **kwargs):
        """Initializes the client entity instance."""
        BackendEntity.__init__(self, config, delegate, verbose)
        for (key, value) in kwargs.items():
            self.__setattr__(key, value)
        if not self.delegateto.country:
            self.delegateto.country = self._get_default_country()
        self.validate()

    def _client_mail(self):
        """Mails a summary about the creation of the client."""
        text = get_template(self.config['mailtemplates'],
                            self.config['client.create.mail']).substitute({
                'firstname': self.delegateto.firstname,
                'lastname': self.delegateto.lastname,
                'email': self.delegateto.email,
                'address1': self.delegateto.address1,
                'zipcode': self.delegateto.zip,
                'city': self.delegateto.city,
                'phone': self.delegateto.phone})
        subject = get_template_string(
            self.config['client.create_subject']).substitute({
                'firstname': self.delegateto.firstname,
                'lastname': self.delegateto.lastname})
        self.send_mail(subject, text)
        
    def create_hook(self, session):
        """Actions to perform when a client is created."""
        self._client_mail()

    def delete_hook(self, session):
        """Actions to perform when a client is deleted."""
        if self.delegateto.sysusers:
            raise CannotDeleteError(
                self.delegateto,
                "it still has the following system users assigned: %s" % (
                ", ".join([sysuser.username for sysuser in \
                           self.delegateto.sysusers])))

    def _get_default_country(self):
        """Gets the default country."""
        return self.config['client.defaultcountry']


class ClientHandler(BackendEntityHandler):
    """BackendEntityHandler for Client entities."""

    def __init__(self, config, verbose = False):
        BackendEntityHandler.__init__(self, ClientEntity, Client, config,
                                      verbose)
