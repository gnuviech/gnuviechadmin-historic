#!/usr/bin/env python
# -*- python -*-
# -*- coding: utf-8 -*-
#
# Copyright (C) 2009 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""
Parse the configuration and setup logging.
"""

from sys import argv
from os import getcwd
from os.path import isfile
from paste.deploy import appconfig
from logging.config import fileConfig

if len(argv) > 1 and isfile(argv[1]):
    CONFIGFILE = argv[1]
    del argv[1]
else:
    CONFIGFILE = 'development.ini'

CONFIG = appconfig('config:%s' % CONFIGFILE, relative_to=getcwd())
fileConfig(CONFIGFILE, CONFIG)
