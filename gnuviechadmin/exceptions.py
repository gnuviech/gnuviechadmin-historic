# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""This file defines the gnuviechadmin specific exception types."""


class GnuviechadminError(Exception):
    """This is the base class for domain specific exceptions of
    Gnuviechadmin."""
    pass


class MissingFieldsError(GnuviechadminError):
    """This exception should be raised when a required field of a data
    class is missing."""

    def __init__(self, missingfields):
        GnuviechadminError.__init__(self)
        self.missing = missingfields

    def __str__(self):
        return "the fields %s are missing." % (repr(self.missing))


class CreationFailedError(GnuviechadminError):
    """This exception should be raised if a business object could not
    be created."""

    def __init__(self, classname, cause = None):
        GnuviechadminError.__init__(self)
        self.classname = classname
        self.cause = cause

    def __str__(self):
        msg = "Creating an instance of class %s failed." % (self.classname)
        if self.cause:
            msg += " The reason is %s." % (str(self.cause))
        return msg


class DeleteFailedError(GnuviechadminError):
    """This exception should be raise if a business object coild not
    be deleted."""

    def __init__(self, classname, cause = None):
        GnuviechadminError.__init__(self)
        self.classname = classname
        self.cause = cause

    def __str__(self):
        msg = "Deleting an instance of class %s failed." % (self.classname)
        if self.cause:
            msg += " The reason is %s." % (str(self.cause))
        return msg


class ValidationFailedError(GnuviechadminError):
    """This exception should be raised if the validation of a business
    object failed."""

    def __init__(self, instance, cause = None):
        GnuviechadminError.__init__(self)
        self.instance = instance
        self.cause = cause

    def __str__(self):
        msg = "Validating %s failed." % (str(self.instance))
        if self.cause:
            msg += " The reason is %s." % (str(self.cause))
        return msg


class CannotDeleteError(GnuviechadminError):
    """This exception should be raised if an entity cannot be deleted
    because of some unmatched precondition."""

    def __init__(self, instance, cause = None):
        GnuviechadminError.__init__(self)
        self.instance = instance
        self.cause = cause

    def __str__(self):
        msg = "Cannot delete %s." % (str(self.instance))
        if self.cause:
            msg += " The reason is %s." % (str(self.cause))
        return msg
