# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

"""This module provides some functions for password handling."""
import crypt
import crack
import random
import logging

LOG = logging.getLogger(__name__)

_PWCHARS = []
for _pair in (('0', '9'), ('A', 'Z'), ('a', 'z')):
    _PWCHARS.extend(range(ord(_pair[0]), ord(_pair[1])))
_SALTCHARS = [_char for _char in _PWCHARS]
for _char in "-+/*_@":
    _PWCHARS.append(ord(_char))


def generatepassword(minlength = 8, maxlength = 12):
    """Generates a new random password with a given length.

    The generated password has a length between minlength and maxlength.

    Keyword arguments:
    `minlength` -- minimum length of the generated password
    `maxlength` -- the maximum length of the generated password
    """
    return "".join([chr(letter) for letter in \
                    random.sample(_PWCHARS,
                                  random.randint(minlength, maxlength))])


def checkpassword(password):
    """Checks the password with cracklib.

    The password is returned if it is good enough. Otherwise None is
    returned.

    Arguments:
    `password` -- the password to check
    """
    try:
        return crack.VeryFascistCheck(password)
    except ValueError, ve:
        LOG.info("Weak password: %s", ve)
    return None


def md5_crypt_password(password):
    """Hashes the given password with MD5 and a random salt value.

    A password hashed with MD5 and a random salt value is returned.

    Arguments:
    `password` -- the password to hash
    """
    salt = "".join([chr(letter) for letter in \
                    random.sample(_SALTCHARS, 8)])
    return crypt.crypt(password, '$1$' + salt)


def get_pw_tuple(password = None):
    """Gets a valid (password, hashvalue) tuple.

    The tuple consists of a password and a md5 hash of the same
    password. If a `password` is given it is checked and if it is too
    weak replaced by a generated one.

    """
    while password == None or checkpassword(password) == None:
        password = generatepassword()
    return (password, md5_crypt_password(password))


def validate_password(hashvalue, password):
    """Validates whether the given clear text `password` matches the
    given `hashvalue`.
    """
    return hashvalue == crypt.crypt(password, hashvalue)
