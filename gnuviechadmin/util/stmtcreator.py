#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2007, 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$

"""This is an SQL statement creator as a prototype for mail account
creation."""

if __name__ == '__main__':
    from gnuviechadmin.util.passwordutils import get_pw_tuple
    import sys

    for line in sys.stdin.readlines():
        parts = line.split()
        if len(parts) < 4:
            raise ValueError("""lines must consist of the elements:
email@domain username uid domainid""")
        (email, domain) = parts[0].split("@")
        username = parts[1][0:5]
        pwtuple = get_pw_tuple()
        print "INSERT INTO mailpassword " + \
            "(id, clearpass, cryptpass, uid, gid, home, spamcheck) " + \
            "VALUES " + \
            "('%s', '%s', '%s', %d, %d, '/home/mail/%s/%s', 'false');" % (
            parts[1], pwtuple[0], pwtuple[1], int(parts[2]), 119,
            username, parts[1])
        print "INSERT INTO mailaddress (domainid, email, target) " + \
            "VALUES (%d, '%s', '%s');" % (
            int(parts[3]), email, parts[1])
