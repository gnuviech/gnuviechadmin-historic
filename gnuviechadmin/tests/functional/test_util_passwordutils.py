# -*- coding: utf-8 -*-
#
# Copyright (C) 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""Unit tests for gnuviechadmin.util.passwordutils.

"""
from gnuviechadmin.util import passwordutils
from unittest import TestCase
import crypt


class TestPasswordUtils(TestCase):
    weakpw = "test"
    strongpw = "Str0ng#l33tP4assword"

    def test_generatepassword(self):
        password = passwordutils.generatepassword()
        self.assert_(password is not None)

    def test_checkpassword(self):
        weakcheck = passwordutils.checkpassword(self.weakpw)
        self.assertEqual(weakcheck, None,
                     "checking a weak password should return ''None''." + \
                             "%s was returned." % weakcheck)
        strongcheck = passwordutils.checkpassword(self.strongpw)
        self.assertEqual(self.strongpw, strongcheck,
                         'checking a strong password should return the ' + \
                             ' the password. %s was returned.' % strongcheck)

    def test_md5_crypt_password(self):
        encrypted = passwordutils.md5_crypt_password(self.strongpw)
        self.assertNotEqual(self.strongpw, encrypted)
        self.assertEqual(encrypted, crypt.crypt(self.strongpw, encrypted))

    def test_get_pw_tuple(self):

        def check_tuple(pwtuple):
            self.assertEqual(len(pwtuple), 2)
            self.assertNotEqual(pwtuple[0], None)
            self.assert_(len(pwtuple[0]) >= 8)
            self.assertEqual(pwtuple[1], crypt.crypt(pwtuple[0], pwtuple[1]))
        check_tuple(passwordutils.get_pw_tuple())
        weakpwtuple = passwordutils.get_pw_tuple(self.weakpw)
        check_tuple(weakpwtuple)
        self.assertNotEqual(self.weakpw, weakpwtuple[0])
        strongpwtuple = passwordutils.get_pw_tuple(self.strongpw)
        check_tuple(strongpwtuple)
        self.assertEqual(self.strongpw, strongpwtuple[0])

    def test_validate_password(self):
        hashed = passwordutils.md5_crypt_password(self.strongpw)
        self.assertEqual(passwordutils.validate_password(hashed,
                                                         self.strongpw), True)
        self.assertEqual(passwordutils.validate_password(self.weakpw,
                                                         self.strongpw), False)
