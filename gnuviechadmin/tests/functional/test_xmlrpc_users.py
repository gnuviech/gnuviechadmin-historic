# -*- coding: utf-8 -*-
#
# Copyright (C) 2008 by Jan Dittberner.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
# USA.
#
# Version: $Id$
"""Unit tests for gnuviechadmin.xmlrpc.users.

"""
from gnuviechadmin.xmlrpc.users import ClientUserProvider, \
  MailuserUserProvider, SysuserUserProvider, GVAUsers
from authkit.users import AuthKitNoSuchUserError
from unittest import TestCase


class TestClientUserProvider(TestCase):
    subject = None

    def setUp(self):
        self.subject = ClientUserProvider(None)

    def test_user(self):
        user = self.subject.user('dummy')
        self.assertEquals(user['roles'], ['client'])
        try:
            user = self.subject.user('nodummy')
            self.fail('should have raised AuthKitNoSuchUserError.')
        except AuthKitNoSuchUserError, e:
            pass

    def test_list_roles(self):
        self.assertEquals(self.subject.list_roles(), ['client'])


class TestMailuserUserProvider(TestCase):
    subject = None

    def setUp(self):
        self.subject = MailuserUserProvider(None)

    def test_user(self):
        try:
            user = self.subject.user('dummy')
            self.fail('should have raised AuthKitNoSuchUserError.')
        except AuthKitNoSuchUserError, e:
            pass

    def test_list_roles(self):
        self.assertEquals(self.subject.list_roles(), ['mailuser'])

class TestSysuserUserProvider(TestCase):
    subject = None

    def setUp(self):
        self.subject = SysuserUserProvider(None)

    def test_user(self):
        try:
            user = self.subject.user('dummy')
            self.fail('should have raised AuthKitNoSuchUserError.')
        except AuthKitNoSuchUserError, e:
            pass

    def test_list_roles(self):
        self.assertEquals(self.subject.list_roles(), ['sysuser'])


class TestGVAUsers(TestCase):
    subject = None

    def setUp(self):
        self.subject = GVAUsers(None, [ClientUserProvider,
                                       MailuserUserProvider,
                                       SysuserUserProvider])

    def test_list_roles(self):
        roles = self.subject.list_roles()
        self.assertEquals(roles, ['client', 'mailuser', 'sysuser'])

    def test_role_exists(self):
        self.assertFalse(self.subject.role_exists('dummy'))
        for role in ('client', 'mailuser', 'sysuser'):
            self.assertTrue(self.subject.role_exists(role))

    def test_user(self):
        try:
            self.subject.user('nouser')
            self.fail('should have raised AuthKitNoSuchUserError.')
        except AuthKitNoSuchUserError, e:
            pass
        user = self.subject.user('dummy')
        print user
