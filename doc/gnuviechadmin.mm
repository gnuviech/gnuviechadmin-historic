<map version="0.7.1">
<node TEXT="gnuviechadmin">
<node TEXT="Kundenverwaltung" POSITION="right">
<node TEXT="Kunde erfassen"/>
<node TEXT="Adressdaten &#xe4;ndern"/>
<node TEXT="Webuser anlegen">
<node TEXT="Mail mit Login"/>
<node TEXT="home mit sshchroot-Umgebung"/>
<node TEXT="quota setzen"/>
</node>
</node>
<node TEXT="Domain anlegen" POSITION="right">
<node TEXT="Kundenzuordnung"/>
<node TEXT="DNS-Zone anlegen">
<node TEXT="aus Template"/>
</node>
<node TEXT="Apache-Konfiguration anlegen">
<node TEXT="Frameweiterleitung"/>
<node TEXT="Defaultseite"/>
<node TEXT="Webuser zuordnen"/>
</node>
</node>
<node TEXT="Domainverwaltung" POSITION="right">
<node TEXT="Mailaccounts verwalten">
<node TEXT="Mailaccounts anlegen">
<node TEXT="Mail mit Accountdaten"/>
</node>
<node TEXT="Mailpassw&#xf6;rter &#xe4;ndern">
<node TEXT="Mail mit Accountdaten"/>
</node>
<node TEXT="Mailadressen anlegen">
<node TEXT="auf Account ablegen"/>
<node TEXT="an Adresse(n) weiterleiten"/>
<node TEXT="Mail mit Weiterleitungsdaten"/>
</node>
</node>
<node TEXT="Apache-Einstellungen &#xe4;ndern">
<node TEXT="Spezialeinstellungen"/>
<node TEXT="Subdomains mit Extra-Documentroot"/>
</node>
<node TEXT="SCP-Passwort &#xe4;ndern"/>
<node TEXT="mehrere Domains auf gleichen SCP-Nutzer"/>
<node TEXT="Selfservice-Funktionen"/>
</node>
<node TEXT="Rechnungsmodul" POSITION="left"/>
</node>
</map>
