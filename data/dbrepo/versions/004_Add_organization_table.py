from sqlalchemy import MetaData, Table, Column, Integer, Unicode, ForeignKey
from migrate import migrate_engine
from migrate.changeset.schema import create_column
from gnuviechadmin.config import CONFIG

dbschema = None
if 'database.schema' in CONFIG:
    dbschema = CONFIG['database.schema']

meta = MetaData(migrate_engine)
client = Table('client', meta, schema = dbschema, autoload = True)
organization = Table(
    'organization', meta,
    Column('organizationid', Integer, primary_key = True),
    Column('name', Unicode(200), nullable = False, unique = True),
    schema = dbschema,
    useexisting = True)

def upgrade():
    organization.create()
    col = Column('organizationid', Integer,
                 ForeignKey(organization.c.organizationid),
                 nullable = True)
    create_column(col, client)


def downgrade():
    client.c.organizationid.drop()
    organization.drop()
