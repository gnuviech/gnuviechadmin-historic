from sqlalchemy import *
from migrate import *
from gnuviechadmin.config import CONFIG

dbschema = None
if 'database.schema' in CONFIG:
    dbschema = CONFIG['database.schema']

meta = MetaData(migrate_engine)
domain = Table('domain', meta, schema = dbschema, autoload = True)
mailaccount = Table(
    'mailaccount', meta,
    Column('mailaccountid', Integer, primary_key = True),
    Column('domainid', Integer, ForeignKey(domain.c.domainid),
           nullable = False),
    Column('mailaccount', String(12), nullable = False, unique = True),
    Column('clearpass', String(64)),
    Column('cryptpass', String(34)),
    Column('uid', Integer, nullable = False),
    Column('gid', Integer, nullable = False),
    Column('home', String(128), nullable = False),
    Column('spamcheck', Boolean, nullable = False, default = False),
    Column('sajunkscore', Integer),
    schema = dbschema)
mailaddress = Table(
    'mailaddress', meta,
    Column('mailaddressid', Integer, primary_key = True),
    Column('domainid', Integer, ForeignKey(domain.c.domainid),
           nullable = False),
    Column('email', String(255), nullable = False),
    UniqueConstraint('email', 'domainid'),
    schema = dbschema)
mailtarget = Table(
    'mailtarget', meta,
    Column('mailtargetid', Integer, primary_key = True),
    Column('mailaddressid', Integer, ForeignKey(mailaddress.c.mailaddressid),
           nullable = False),
    Column('target', String(128), nullable = False),
    UniqueConstraint('target', 'mailaddressid'),
    schema = dbschema)


def upgrade():
    mailaccount.create()
    mailaddress.create()
    mailtarget.create()


def downgrade():
    mailtarget.drop()
    mailaddress.drop()
    mailaccount.drop()
