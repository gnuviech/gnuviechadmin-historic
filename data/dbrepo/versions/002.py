from sqlalchemy import *
from migrate import *
from gnuviechadmin.config import CONFIG

dbschema = None
if 'database.schema' in CONFIG:
    dbschema = CONFIG['database.schema']

meta = MetaData(migrate_engine)
client = Table(
    'client', meta,
    Column('clientid', Integer, primary_key=True),
    Column('title', Unicode(10)),
    Column('firstname', Unicode(64), nullable=False),
    Column('lastname', Unicode(64), nullable=False),
    Column('address1', Unicode(64), nullable=False),
    Column('address2', Unicode(64)),
    Column('zip', String(7), nullable=False),
    Column('city', Unicode(64), nullable=False),
    Column('country', String(5), nullable=False),
    Column('phone', String(32), nullable=False),
    Column('mobile', String(32)),
    Column('fax', String(32)),
    Column('email', String(64), unique=True, nullable=False),
    schema = dbschema)
sysuser = Table(
    'sysuser', meta,
    Column('sysuserid', Integer, primary_key=True),
    Column('username', String(12), nullable=False, unique=True),
    Column('usertype', Integer, nullable=False, default=0, index=True),
    Column('home', String(128)),
    Column('shell', Boolean, nullable=False, default=False),
    Column('clearpass', String(64)),
    Column('md5pass', String(34)),
    Column('clientid', Integer, ForeignKey(client.c.clientid),
           nullable=False),
    Column('sysuid', Integer, nullable=False, unique=True),
    Column('lastchange', DateTime, default=func.now()),
    schema = dbschema)
domain = Table(
    'domain', meta,
    Column('domainid', Integer, primary_key=True),
    Column('name', String(255), nullable=False, unique=True),
    Column('master', String(20)),
    Column('last_check', Integer),
    Column('type', String(6), nullable=False),
    Column('notified_serial', Integer),
    Column('sysuserid', Integer, ForeignKey(sysuser.c.sysuserid),
           nullable=False),
    schema = dbschema)
record = Table(
    'record', meta,
    Column('recordid', Integer, primary_key=True),
    Column('domainid', Integer, ForeignKey(domain.c.domainid),
           nullable=False),
    Column('name', String(255)),
    Column('type', String(6)),
    Column('content', String(255)),
    Column('ttl', Integer),
    Column('prio', Integer),
    Column('change_date', Integer),
    schema = dbschema)
supermaster = Table(
    'supermaster', meta,
    Column('ip', String(25), nullable=False),
    Column('nameserver', String(255), nullable=False),
    Column('account', Integer, ForeignKey(sysuser.c.sysuserid),
           nullable=False),
    schema = dbschema)


def upgrade():
    client.create()
    sysuser.create()
    domain.create()
    record.create()
    supermaster.create()


def downgrade():
    supermaster.drop()
    record.drop()
    domain.drop()
    sysuser.drop()
    client.drop()
